# Generate and Deploy API documentations to GitLab Pages

[Live Demo](http://malik-aliyev-94.gitlab.io/)

## Dependencies

- [Redoc OpenAPI/Swagger-generated API Reference Documentation](https://github.com/Redocly/redoc)
- [Create react app](https://create-react-app.dev/)

## Steps to include new API Spec
1. Go to /client/public folder
2. Open docs.json file and add new item
```
{
    "title": "Simple API",
    "path": "https://raw.githubusercontent.com/OAI/OpenAPI-Specification/master/examples/v2.0/json/api-with-examples.json"
}
```
3. To generate API from local file, add file to ```/client/public/specs``` folder
```
{
    "title": "Payments API",
    "path": "/specs/swagger-payments.json"
}
```

## To Do

- Create ```specs``` folder and ```docs.json``` config file at the root
- Copy specs folder and config file to ```/client/public/specs``` on deploy
- Improve UI
