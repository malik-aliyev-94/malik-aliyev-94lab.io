import { useEffect, useState } from 'react';
import { RedocStandalone } from 'redoc';
import logo from './logo.svg';
import './App.css';

function App() {

  const [ docs, setDocs ] = useState([]);
  const [ spec, setSpec ] = useState(null);

  useEffect(() => {
    const fetchDocs = async () => {
      const response = await fetch('/docs.json');
      const responseJSON = await response.json();
      setDocs(responseJSON);
    }

    fetchDocs();
  }, [])

  useEffect(() => {
    const fetchSpec = async (path) => {
      const response = await fetch(path);
      const responseJSON = await response.json();
      setSpec(responseJSON);
    }

    const s = new URLSearchParams(window.location.search).get("doc")
    if (s !== undefined && docs[s]) {
      fetchSpec(docs[s].path)
    }
  }, [docs])

  return (
    <div>
      <h1 style={{padding: 20, paddingBottom: 0}}>Select API</h1>
      {!spec ? 
      <div className="catalog">
        <ul className="directory">
        {docs.map((doc, index) => (
          <li key={index}>
            <a href={`?doc=${index}`} key={index}>{doc.title || doc.path}</a>
          </li>
        ))}
        </ul>
      </div>
      :
      <div style={{paddingLeft: 200}}>
        <div style={{width: 260, backgroundColor: '#263238', position: 'fixed', left: 0, top: 0, width: 200, height: '100%'}}>
          <ul className="directory">
          {docs.map((doc, index) => (
            <li key={index}>
              <a href={`?doc=${index}`} key={index}>{doc.title || doc.path}</a>
            </li>
          ))}
          </ul>
        </div>
        <RedocStandalone 
          // specUrl="http://petstore.swagger.io/v2/swagger.json"
          specUrl={spec}
        />
      </div>}

    </div>
  );
}

export default App;
